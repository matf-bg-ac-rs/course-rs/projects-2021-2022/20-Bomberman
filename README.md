# Project Bomberman

Igrac ima zadatak da pronadje skrivena vrata iza nekog od zidova i nakon unistavanja poslednjeg duha prodje kroz njih i predje na naredni, tezi nivo. 
Prepreke u igri predstavljaju duhovi kojima je za cilj postavljeno da uniste igraca.
Igrac mora da vodi racuna da ne dodje u direktan kontakt sa duhom inace gubi zivot a kasnije i igru.
Igrac ima mogucnost da postavlja bombu (bombe*) koja se aktivira nakon odredjenog vremena i unistavaju sve pred sobom (osim vrata) u svim dozvoljenim smerovima (gore, dole, levo, desno) po jedno polje (ili vise od jednog polja*).
Igrac mora da pazi da ne dodje u kontakt sa eksplozijom jer gubi zivot.
U slucaju izgubljenih svih zivota, igra se zavrsava.
*powerUp
Tokom rusenja zidova igrac moze otkriti powerUp koji mu unapredjuje sposobnosti ili dodaje nove.
Igra je zamisljena da ima razlicite tezine, koje igrac bira na pocetku igre.
Igra se zavrsava nakon uspesnih prelaska svih nivoa prolaskom kroz vrata poslednjeg nivoa.
Nakon zavrsetka, igrac dobija rezultat koji se cuva na bodovnoj listi najboljih rezultata.

## Demo igre moze se naci na adresi
https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2021-2022/20-Bomberman/-/blob/master/specifikacija/bomberman.mkv

## Developers

- [Sanja Mijatović, 196/2016](https://gitlab.com/sanja10)
- [Sanja Radulović, 15/2016](https://gitlab.com/Sanja-Radulovic)
- [Filip Ranđelović, 447/2019](https://gitlab.com/filip.randjelovic)
- [Marko Vićentijević, 123/2016](https://gitlab.com/mich2702)

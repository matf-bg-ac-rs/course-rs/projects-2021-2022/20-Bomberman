#include <src/bomb.hpp>
#include "bombermantimer.hpp"
#include <QDebug>

BombermanTimer::BombermanTimer(int time)
{
    _timer = new QTimer(this);
    _time = time;
}

void BombermanTimer::startTimer()
{
    _timer->start(_time);
}

BombermanTimer::~BombermanTimer()
{
    delete _timer;
//    qDebug() << "~BombermanTimer";
}

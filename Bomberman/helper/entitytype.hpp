#ifndef ENTITYTYPE_HPP
#define ENTITYTYPE_HPP

enum class EntityType
{
    BOMBERMAN,
    GHOST,
    BOMB,
    EXPLOSION,
    WALL,
    POWERUP,
    DOOR,
    FIELD
};

#endif // ENTITYTYPE_HPP

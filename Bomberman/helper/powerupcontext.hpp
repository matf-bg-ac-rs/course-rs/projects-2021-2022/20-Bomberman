#ifndef POWERUPCONTEXT_HPP
#define POWERUPCONTEXT_HPP


class PowerUpContext
{
public:
    PowerUpContext();

    int getBombExplotionRange() const;
    void addOneExplotionRange();
    void removeExplotionRange();

    int getNumberOfBombs() const;
    void addOneBomb();
    void removeBomb();

    ~PowerUpContext();

private:
    int _bombExplotionRange = 1;
    int _numberOfBombs = 1;
};

#endif // POWERUPCONTEXT_HPP

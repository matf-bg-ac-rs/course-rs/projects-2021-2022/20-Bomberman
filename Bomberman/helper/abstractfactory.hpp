#ifndef ABSTRACTFACTORY_HPP
#define ABSTRACTFACTORY_HPP

#include <src/interfaces/factory.hpp>

#include <QMap>



class AbstractFactory : public Factory
{
public:
    AbstractFactory();

private:
    QMap<EntityType, Factory*> factories;
};

#endif // ABSTRACTFACTORY_HPP

#ifndef GAMECONTEXT_HPP
#define GAMECONTEXT_HPP

#include "powerupcontext.hpp"

#include <QTextStream>



class GameContext
{
public:
    GameContext();

    int getLevel() const;
    void incrementLevel();

    int getScore() const;
    void updateScore(const int score);

    int getCurrentSpeed() const;
    void addToCurrentSpeed(const int speed);
    void removeFromCurrentSpeed(const int speed);

    int getNumberOfLives() const;
    void addOneLive();
    void removeLive();

    int getBombExplotionRange() const;
    void addOneExplotionRange();
    void removeExplotionRange();

    int getNumberOfBombs() const;
    void addOneBomb();
    void removeBomb();

    ~GameContext();

private:

    int _level = 1;
    int _score = 0;

    int _currentSpeed = 1000;
    int _lives = 3;
    int _bombExplotionRange = 1;
    int _numberOfBombs = 1;

};

#endif // GAMECONTEXT_HPP

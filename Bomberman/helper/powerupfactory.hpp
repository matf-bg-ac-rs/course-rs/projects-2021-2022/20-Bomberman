#ifndef POWERUPFACTORY_HPP
#define POWERUPFACTORY_HPP

#include <src/interfaces/powerUp.hpp>

class PowerUpFactory
{
public:
    PowerUpFactory();
    static PowerUp* getFactoryObject(QPoint *point);

};

#endif // POWERUPFACTORY_HPP

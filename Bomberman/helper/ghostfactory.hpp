#ifndef GHOSTFACTORY_HPP
#define GHOSTFACTORY_HPP

#include <src/interfaces/factory.hpp>
#include <src/interfaces/ghost.hpp>

class GhostFactory : public Factory
{
public:
    GhostFactory(); // FINISH FACTORY
    static Ghost* getFactoryObject(QPoint *point);
};

#endif // GHOSTFACTORY_HPP

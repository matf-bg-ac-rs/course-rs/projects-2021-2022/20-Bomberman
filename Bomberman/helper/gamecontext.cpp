#include "gamecontext.hpp"
#include <QDebug>
#include <include/gameManager.hpp>
#include <include/mainwindow.hpp>

GameContext::GameContext()
{

}

int GameContext::getLevel() const
{
    return _level;
}

void GameContext::incrementLevel()
{
    if (_level > 5) {
        GameManager::getInstance()->setExitCode(0);

        qInfo() << "Final score is: " << _score;
    } else {
        _level++;
        qInfo() << "added level, num of levels: " << _level;
    }
    MainWindow::activateReboot();
}

int GameContext::getScore() const
{
    return _score;
}

void GameContext::updateScore(const int score)
{
    _score += score;
    qInfo() << "_score level, num of _score: " << _score;
}

int GameContext::getCurrentSpeed() const
{
    return _currentSpeed;
}

void GameContext::addToCurrentSpeed(const int speed)
{
    if (_currentSpeed < 3000) {
        _currentSpeed += speed;
        qInfo() << "added _currentSpeed, num of _currentSpeed: " << _currentSpeed;
    }
}

void GameContext::removeFromCurrentSpeed(const int speed)
{
    if (_currentSpeed > 0) {
        _currentSpeed -= speed;
        GameManager::getInstance()->getBomberman()->destroyTimer();
        GameManager::getInstance()->getBomberman()->setSpeed(_currentSpeed);
        GameManager::getInstance()->getBomberman()->createTimer();
        qInfo() << "removed _currentSpeed, num of _currentSpeed: " << _currentSpeed;
    }
}

int GameContext::getNumberOfLives() const
{
    return _lives;
}

void GameContext::addOneLive()
{
    if (_lives < 5) {
        _lives++;
        qInfo() << "added _live, num of _live: " << _lives;
    }
}

void GameContext::removeLive()
{
    if (_lives > 0) {
        _lives--;
        qInfo() << "removed _live, num of _live: " << _lives;
    } else {
        GameManager::getInstance()->setExitCode(0);
        qInfo() << "Final score is: " << _score;
        MainWindow::activateReboot();
    }
}

int GameContext::getBombExplotionRange() const
{
    return _bombExplotionRange;
}

void GameContext::addOneExplotionRange()
{
    if (_bombExplotionRange < 5) {
        _bombExplotionRange++;
        qInfo() << "added _bombExplotionRange, num of _bombExplotionRange: " << _bombExplotionRange;
    }
}

void GameContext::removeExplotionRange()
{
    if(_bombExplotionRange > 0) {
        _bombExplotionRange--;
        qInfo() << "removed _bombExplotionRange, num of _bombExplotionRange: " << _bombExplotionRange;
    }
}

int GameContext::getNumberOfBombs() const
{
    return _numberOfBombs;
}

void GameContext::addOneBomb()
{
    if (_numberOfBombs < 5) {
        _numberOfBombs++;
        qInfo() << "added _numberOfBombs, num of _numberOfBombs: " << _numberOfBombs;
    }
}

void GameContext::removeBomb()
{
    if (_numberOfBombs > 0) {
        _numberOfBombs--;
        qInfo() << "removed _numberOfBombs, num of _numberOfBombs: " << _numberOfBombs;
    }
}

GameContext::~GameContext()
{

}

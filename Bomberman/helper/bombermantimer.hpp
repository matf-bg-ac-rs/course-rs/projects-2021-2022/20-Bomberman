#ifndef BOMBERMANTIMER_H
#define BOMBERMANTIMER_H

#include <QObject>
#include <QTimer>
#include <QDebug>

class BombermanTimer : public QObject
{
    Q_OBJECT

public:
    BombermanTimer(int time = 1000);
    QTimer *_timer;
    void startTimer();
    ~BombermanTimer();

private:
    int _time;

};

#endif // BOMBERMANTIMER_H

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    helper/abstractfactory.cpp \
    helper/bombermantimer.cpp \
    helper/gamecontext.cpp \
    helper/ghostfactory.cpp \
    helper/powerupfactory.cpp \
    src/bomb.cpp \
    src/bombPowerUp.cpp \
    src/bomberman.cpp \
    src/door.cpp \
    src/entity.cpp \
    src/explosion.cpp \
    src/explosionPowerUp.cpp \
    src/fastghost.cpp \
    src/field.cpp \
    src/gameManager.cpp \
    src/healthPowerUp.cpp \
    src/interfaces/actionable.cpp \
    src/interfaces/factory.cpp \
    src/interfaces/ghost.cpp \
    src/interfaces/hidden.cpp \
    src/interfaces/movable.cpp \
    src/interfaces/powerUp.cpp \
    src/main.cpp \
    src/mainwindow.cpp \
    src/slowGhost.cpp \
    src/speedPowerUp.cpp \
    src/wall.cpp

HEADERS += \
    helper/abstractfactory.hpp \
    helper/bombermantimer.hpp \
    helper/direction.hpp \
    helper/entitytype.hpp \
    helper/gamecontext.hpp \
    helper/ghostfactory.hpp \
    helper/powerupfactory.hpp \
    include/bombPowerUp.hpp \
    include/bomberman.hpp \
    include/door.hpp \
    include/entity.hpp \
    include/explosionPowerUp.hpp \
    include/fastghost.hpp \
    include/field.hpp \
    include/gameManager.hpp \
    include/healthPowerUp.hpp \
    include/mainwindow.hpp \
    include/settings.hpp \
    include/slowGhost.hpp \
    include/speedPowerUp.hpp \
    include/wall.hpp \
    src/bomb.hpp \
    src/explosion.hpp \
    src/interfaces/actionable.hpp \
    src/interfaces/factory.hpp \
    src/interfaces/ghost.hpp \
    src/interfaces/hidden.hpp \
    src/interfaces/movable.hpp \
    src/interfaces/powerUp.hpp

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc \
    resources.qrc

DISTFILES += \
    images/bm.png \
    images/bomb.png \
    images/bombPowerUp.png \
    images/destroyableBlock.png \
    images/explosionPowerUp.png \
    images/fastGhost.png \
    images/field.png \
    images/fire.png \
    images/healthPowerUp.png \
    images/unDestroyableBlock.png

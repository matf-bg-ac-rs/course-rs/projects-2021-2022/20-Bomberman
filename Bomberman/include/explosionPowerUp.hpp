#ifndef EXPLOSIONPOWERUP_HPP
#define EXPLOSIONPOWERUP_HPP

#include "../src/interfaces/powerUp.hpp"

class ExplosionPowerUp : public PowerUp
{
public:
    ExplosionPowerUp(QPoint position, QObject *parent = nullptr);
    ~ExplosionPowerUp();

    void usePower() override;
};


#endif // EXPLOSIONPOWERUP_HPP

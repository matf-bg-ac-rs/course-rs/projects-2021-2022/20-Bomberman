#ifndef BOMBERMAN_H
#define BOMBERMAN_H

#include <QDebug>
#include <helper/bombermantimer.hpp>
#include <src/interfaces/movable.hpp>

#include <src/interfaces/actionable.hpp>

class Bomberman: public Movable
{
public:
    Bomberman(QPoint position, QObject *parent = nullptr, int speed = 1000);
    Actionable* onAction(Actionable *actionable) override;
    void setAvailableToMove(bool availableToMove);
    bool isAvailableToMove();
//    void createTimer();
    ~Bomberman();

public slots:
    void useTimer() override;

private:
    bool _availableToMove = true;
};

#endif // BOMBERMAN_H

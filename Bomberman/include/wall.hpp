#ifndef WALL_HPP
#define WALL_HPP

#include <src/interfaces/actionable.hpp>

class Wall : public Actionable
{
    public:
    Wall(QPoint position, QObject *parent = nullptr);
    Actionable* onAction(Actionable *actionable) override;
    ~Wall();
};

#endif // WALL_HPP

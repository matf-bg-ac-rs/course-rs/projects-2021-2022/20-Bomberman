#ifndef UNUSABLEFIELD_HPP
#define UNUSABLEFIELD_HPP

#include <include/field.hpp>

class UnusableField : public Field {

public:
      UnusableField(QPoint position);

      ~UnusableField();

};

#endif // UNUSABLEFIELD_HPP


//inline UnusableField::UnusableField(QPoint position) : Field(std::move(position))
//{}


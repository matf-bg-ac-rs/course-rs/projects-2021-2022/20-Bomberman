#ifndef FASTGHOST_HPP
#define FASTGHOST_HPP

#include "../src/interfaces/ghost.hpp"
#include <QDebug>

class FastGhost : public Ghost
{

public:
    FastGhost(QPoint position, QObject *parent = nullptr, int movement = 1000);
    ~FastGhost();

public slots:
    void useTimer() override;

private:
    Direction lastTry = Direction::LEFT;
    Direction randomDirection();
};

#endif // FASTGHOST_HPP

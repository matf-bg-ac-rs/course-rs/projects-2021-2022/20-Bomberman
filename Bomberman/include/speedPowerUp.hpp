#ifndef SPEEDPOWERUP_HPP
#define SPEEDPOWERUP_HPP

#include "../src/interfaces/powerUp.hpp"

class SpeedPowerUp : public PowerUp
{
public:
    SpeedPowerUp(QPoint position, QObject *parent = nullptr);
    ~SpeedPowerUp();

    void usePower()  override;

private:
    int _speedConstant = 200;
};


#endif // SPEEDPOWERUP_HPP

#ifndef DOOR_HPP
#define DOOR_HPP

#include "../src/interfaces/hidden.hpp"

class Door : public Hidden
{
public:
    Door(QPoint position, QObject *parent = nullptr);
    ~Door();

    Actionable* onAction(Actionable *actionable) override;
};

#endif // DOOR_HPP

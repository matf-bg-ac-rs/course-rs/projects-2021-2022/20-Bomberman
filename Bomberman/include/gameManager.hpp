#ifndef GAMEMANAGER_HPP
#define GAMEMANAGER_HPP

#include <QGraphicsPixmapItem>
#include <QObject>
#include <QGraphicsScene>
#include <QKeyEvent>
#include <QSet>
#include <QList>
#include <QVector>
#include <QSharedPointer>
#include <QString>
#include <ui_mainwindow.h>

#include <include/bomberman.hpp>
#include <include/entity.hpp>
#include <include/field.hpp>
#include <include/slowGhost.hpp>

#include <helper/gamecontext.hpp>


class GameManager : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT

public:

    QGraphicsScene * getGameScene();
    static GameManager* getInstance();

    QAction* getActionReboot();
    void setActionReboot(QAction* action);
    void activateReboot();

    Bomberman* getBomberman();

    void init(QGraphicsScene *gameScene, GameContext* gameContext);

    void keyPressEvent(QKeyEvent *event);
    void makeBomb();

    QSet<QString> getAvailableFields() const;
    void insertAvailableField(QString stringPosition);
    void removeAvailableField(QString stringPosition);

    QMap<QString, Field *> getFieldActorsMap();
    Field * getField(QString stringPosition);
    void putFieldActorsinMap(Actionable* actionable);
    void popFieldActorsFromMap(Actionable* actionable);

    void decresePowerUpTime();
    int getPowerUpTime();
    void decreseDoorTime();
    int getDoorTime();
    void decreseGhostCounter();
    int getGhostCounter();

    static QString makeStringPointStatic(QPoint *point);
    static QString makeStringPointStatic(const QPoint *point);

    GameContext* getGameContext();
    void setGameContext(GameContext* gameContext);

    bool resetGameManager();

    bool bombermanKill();

    int getExitCode();
    void setExitCode(int exitCode);

    ~GameManager();

signals:
    void powerUpUsed();

protected:
    GameManager();

private:
    static GameManager* _gameManager;
    QGraphicsScene *_gameScene;
    QAction* _actionReboot;
    int _exitCode = -123456789;

    QMap<QString, Field *> _fieldActorsMap;
    Bomberman *_bomberman;
    QSet<QString> _availableFields;

    GameContext *_gameContext = new GameContext();

    int _ghostCounter;

    int _powerUpTime;
    int _doorTime;

};

#endif // GAMEMANAGER_HPP

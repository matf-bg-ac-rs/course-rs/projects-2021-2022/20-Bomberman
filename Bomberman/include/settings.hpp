#ifndef SETTINGS_HPP
#define SETTINGS_HPP

namespace sizes {
    enum gameStats {
        ROWS = 13,
        COLUMNS = 13,
        FIELDSIZE = 50
    };
}

#endif // SETTINGS_HPP

#ifndef BOMBPOWERUP_HPP
#define BOMBPOWERUP_HPP

#include "../src/interfaces/powerUp.hpp"

class BombPowerUp : public PowerUp
{
public:
    BombPowerUp(QPoint position, QObject *parent = nullptr);
    ~BombPowerUp();

    void usePower() override;
};


#endif // BOMBPOWERUP_HPP

#ifndef FIELD_HPP
#define FIELD_HPP

#include <include/entity.hpp>
#include <queue>
#include <QDebug>
#include <src/interfaces/actionable.hpp>

class Field : public Entity
{
public:
    Field(QPoint position, QObject *parent = nullptr);
    QList<Actionable*> getActionableList() const;
    void removeActionableFromList(Actionable *actionable);
    void addActionableFieldToList(Actionable *actionable);
    bool fieldEntered(Actionable *actionable);
    ~Field();

private:
    QList<Actionable*> _actionableList = *new QList<Actionable*>();
};

#endif // FIELD_HPP

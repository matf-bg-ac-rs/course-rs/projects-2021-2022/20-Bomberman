#ifndef ENTITY_HPP
#define ENTITY_HPP

#include <QObject>
#include <QGraphicsPixmapItem>
#include <helper/entitytype.hpp>

class Entity : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:
    Entity(QPoint position, EntityType entityType, QObject *parent = nullptr);
    QString getStringPosition() const;
    void createStringPosition();
    QPoint getPosition() const;
    EntityType getEntityType() const;
    virtual ~Entity();


    QPoint _position;
private:
    EntityType _entityType;
    QString _stringPosition;
};

#endif // ENTITY_HPP

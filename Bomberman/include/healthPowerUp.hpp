#ifndef HEALTHPOWERUP_HPP
#define HEALTHPOWERUP_HPP

#include "../src/interfaces/powerUp.hpp"

class HealthPowerUp : public PowerUp
{
public:
    HealthPowerUp(QPoint position, QObject *parent = nullptr);
    ~HealthPowerUp();

    void usePower() override;
};

#endif // HEALTHPOWERUP_HPP

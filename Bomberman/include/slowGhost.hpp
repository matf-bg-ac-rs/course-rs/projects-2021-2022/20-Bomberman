#ifndef SLOWGHOST_HPP
#define SLOWGHOST_HPP

#include "../src/interfaces/ghost.hpp"
#include <QDebug>

class SlowGhost : public Ghost
{

public:
    SlowGhost(QPoint position, QObject *parent = nullptr, int speed = 2000);
    ~SlowGhost();

public slots:
    void useTimer() override;

private:
    Direction lastTry = Direction::LEFT;
    Direction randomDirection();
};

#endif // SLOWGHOST_HPP

#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsItem>
#include <QKeyEvent>

#include "gameManager.hpp"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr, GameContext* gameContext = nullptr);
    ~MainWindow();
    void init();
    void init(GameContext* gameContext);
    void keyPressEvent(QKeyEvent *event);
    static int const EXIT_CODE_REBOOT;

    static void activateReboot();

public slots:
    void slotReboot();

//public slots:
//    void bombermanDead();

private:

    Ui::MainWindow *ui;
    QGraphicsView *_view;
    QGraphicsScene *_gameScene;
    GameManager *_gameManager;
};
#endif // MAINWINDOW_HPP

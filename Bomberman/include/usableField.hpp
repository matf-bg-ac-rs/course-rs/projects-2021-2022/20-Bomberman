#ifndef USABLEFIELD_H
#define USABLEFIELD_H

#include <include/field.hpp>

class UsableField : public Field {

public:
    UsableField(QPoint position);

    ~UsableField();

};

#endif // USABLEFIELD_H


//inline UsableField::UsableField(QPoint position) : Field(std::move(position))
//{}


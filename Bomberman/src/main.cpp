#include "../include/mainwindow.hpp"

#include <QApplication>
int main(int argc, char *argv[])
{
//    QApplication a(argc, argv);
//    MainWindow w;
//    w.show();
//    return a.exec();

    int return_from_event_loop_code;
      QPointer<QApplication> app;
      QPointer<MainWindow> main_window;

      GameContext *gc = new GameContext();
      do
      {
        if(app) delete app;
        if(main_window) delete main_window;

        app = new QApplication(argc, argv);
        MainWindow mainWindow(nullptr, gc);
        mainWindow.show();
        return_from_event_loop_code = app->exec();
      }
      while(return_from_event_loop_code == MainWindow::EXIT_CODE_REBOOT);

      return return_from_event_loop_code;
}

#include "../include/mainwindow.hpp"
#include "ui_mainwindow.h"
#include "../include/settings.hpp"
#include <QProcess>

int const MainWindow::EXIT_CODE_REBOOT = -123456789;

MainWindow::MainWindow(QWidget *parent, GameContext *gameContext)
    : QMainWindow(parent)
{
    init(gameContext);
    GameManager::getInstance()->setActionReboot(new QAction( this ));
    GameManager::getInstance()->getActionReboot()->setText( tr("Restart") );
    GameManager::getInstance()->getActionReboot()->setStatusTip( tr("Restarts the application") );
    connect(GameManager::getInstance()->getActionReboot(), SIGNAL (triggered()),this, SLOT (slotReboot()));

}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Space)
    {
        GameManager::getInstance()->makeBomb();
    }
    else
    {
        GameManager::getInstance()->keyPressEvent(event);
    }
}

void MainWindow::activateReboot()
{
    GameManager::getInstance()->getActionReboot()->trigger();
}

void MainWindow::slotReboot()
{
    qApp->exit(GameManager::getInstance()->getExitCode());
    // release the data
}

MainWindow::~MainWindow()
{
    //delete ui;
}

void MainWindow::init()
{
    _gameScene = new QGraphicsScene();
    _gameScene->setSceneRect(0, 0, 650, 650);
    _gameScene->clearFocus();

    _view = new QGraphicsView(_gameScene);
    _view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _view->setFixedSize(650, 650);

    setCentralWidget(_view);
    setWindowTitle(tr("Bomberman"));

    GameContext* gc = new GameContext();

    GameManager::getInstance()->init(_gameScene, gc);


    setFocus();

//    connect(GameManager::getInstance(), &GameManager::bombermanDestroy, this, &MainWindow::slotReboot);


}

void MainWindow::init(GameContext *gameContext)
{
    _gameScene = new QGraphicsScene();
    _gameScene->setSceneRect(0, 0, 650, 650);
    _gameScene->clearFocus();

    _view = new QGraphicsView(_gameScene);
    _view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _view->setFixedSize(650, 650);

    setCentralWidget(_view);
    setWindowTitle(tr("Bomberman"));

    GameManager::getInstance()->init(_gameScene, gameContext);

    setFocus();
}


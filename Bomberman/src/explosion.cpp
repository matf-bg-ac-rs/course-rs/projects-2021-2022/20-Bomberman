#include "explosion.hpp"

#include <QObject>
#include <QPixmap>
#include <QPoint>

#include <include/gameManager.hpp>

Explosion::Explosion(QPoint position, QObject *parent)
    : Actionable(position, EntityType::EXPLOSION, parent)
{
    setPos(position.x(), position.y());
    setPixmap(QPixmap(":/images/images/fire.png"));
    createTimer();
}

Actionable* Explosion::onAction(Actionable *actionable)
{
    switch (actionable->getEntityType()) {
        case EntityType::EXPLOSION:
        case EntityType::DOOR:
        case EntityType::FIELD:
            return nullptr;
        default:
            return actionable;
    }
}

void Explosion::createTimer()
{
    _bombermanTimer = new BombermanTimer();
    connect(_bombermanTimer->_timer, &QTimer::timeout, this, &Explosion::objectDie);
    _bombermanTimer->startTimer();
}

Explosion::~Explosion(){
//    qDebug() << "~Explotion";
    _bombermanTimer->~BombermanTimer();
//    GameManager::getInstance()->popFieldActorsList(this);
    GameManager::getInstance()->popFieldActorsFromMap(this);
    GameManager::getInstance()->insertAvailableField(this->getStringPosition());
}

void Explosion::objectDie()
{
    this->Explosion::~Explosion();
}

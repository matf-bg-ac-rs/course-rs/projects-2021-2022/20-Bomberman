#include "../include/bomberman.hpp"

#include <include/gameManager.hpp>
#include <include/mainwindow.hpp>

#include <src/interfaces/powerUp.hpp>

Bomberman::Bomberman(QPoint position, QObject *parent, int speed)
    : Movable(position, EntityType::BOMBERMAN, parent)
{
    setPos(position.x(), position.y());
    setPixmap(QPixmap(":/images/images/bm.png"));
    setSpeed(speed);
    createTimer();
}

Actionable* Bomberman::onAction(Actionable *actionable)
{
    switch (actionable->getEntityType()) {
    //    case EntityType::BOMBERMAN:
    //        break;
        case EntityType::GHOST:
            return this;
        case EntityType::BOMB:
            return nullptr;
        case EntityType::POWERUP:
            return actionable->onAction(this);
        case EntityType::DOOR:
            if(GameManager::getInstance()->getGhostCounter() == 0) {
                GameManager::getInstance()->getGameContext()->incrementLevel();
            }
            return nullptr;
    //    case EntityType::FIELD:
    //        break;
        case EntityType::EXPLOSION:
            return actionable->onAction(this);
        default:
            return nullptr;
    }
}

void Bomberman::setAvailableToMove(bool availableToMove)
{
    _availableToMove = availableToMove;
}

bool Bomberman::isAvailableToMove()
{
    return _availableToMove;
}

Bomberman::~Bomberman()
{
    GameManager::getInstance()->getGameContext()->removeLive();
    MainWindow::activateReboot();
}

void Bomberman::useTimer()
{
    _availableToMove = true;
    timerStart();

}


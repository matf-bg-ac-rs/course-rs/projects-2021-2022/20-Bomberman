#include "../include/healthPowerUp.hpp"

HealthPowerUp::HealthPowerUp(QPoint position, QObject *parent)
    : PowerUp(position, parent)
{
    setPos(position.x(), position.y());
    setPixmap(QPixmap(":/images/images/healthPowerUp.png"));
}

HealthPowerUp::~HealthPowerUp()
{

}

void HealthPowerUp::usePower()
{
    GameManager::getInstance()->getGameContext()->addOneLive();
}

#include "../include/gameManager.hpp"
#include "../include/settings.hpp"

#include "explosion.hpp"

#include <QMap>
#include <QRandomGenerator>
#include <QString>

#include <include/wall.hpp>
#include <include/bombPowerUp.hpp>
#include <include/door.hpp>
#include <include/mainwindow.hpp>
#include <include/fastghost.hpp>
#include <src/bomb.hpp>
#include <helper/ghostfactory.hpp>
#include <helper/powerupfactory.hpp>

GameManager* GameManager::_gameManager = 0;

GameManager::GameManager()
{

}

GameManager::~GameManager()
{

}

QGraphicsScene *GameManager::getGameScene()
{
    return _gameScene;
}

GameManager* GameManager::getInstance()
{
    if (_gameManager == 0)
    {
        _gameManager = new GameManager();
    }
    return _gameManager;
}



int GameManager::getExitCode()
{
    return _exitCode;
}

void GameManager::setExitCode(int exitCode)
{
    _exitCode = exitCode;
}

QAction *GameManager::getActionReboot()
{
    return _actionReboot;
}

void GameManager::setActionReboot(QAction *action)
{
    _actionReboot = action;
}

Bomberman *GameManager::getBomberman()
{
    return _bomberman;
}

void GameManager::init(QGraphicsScene *gameScene, GameContext* gameContext)
{
    setGameContext(gameContext);

    _gameScene = gameScene;
    _availableFields = *new QSet<QString>;
    for (int i = 0; i < sizes::ROWS; i++)
    {
        for (int j = 0; j < sizes::COLUMNS; j++)
        {
            QPoint tmp(QPoint(i*sizes::FIELDSIZE, j*sizes::FIELDSIZE));
           if (i % 2 != 0 && j % 2 != 0) { // undestroyable walls
               Field *newF = new Field(tmp);
               newF->setPixmap(QPixmap(":/images/images/unDestroyableBlock.png"));
               gameScene->addItem(newF);
               continue;
           }

           if ((i == 0 && j == 0) || (i == 0 && j == 1) || (i == 1 && j == 0)) {
           } else {
               insertAvailableField(GameManager::makeStringPointStatic(&tmp));
           }
           Field *newField = new Field(tmp);

           _fieldActorsMap.insert(GameManager::makeStringPointStatic(&tmp), newField);

           gameScene->addItem(newField);
        }
    }

    _bomberman = new Bomberman(QPoint(0, 0), nullptr, gameContext->getCurrentSpeed());
    // Generate n new walls
    int n = 15;

    QRandomGenerator randomNumber = QRandomGenerator::securelySeeded();

    while(n) {
        QPoint *point = new QPoint(randomNumber.bounded(sizes::COLUMNS) * sizes::FIELDSIZE
                                   , randomNumber.bounded(sizes::ROWS) * sizes::FIELDSIZE);
        QString stringPoint = GameManager::makeStringPointStatic(point);
        if(getAvailableFields().contains(stringPoint)) {
            Wall *wall = new Wall(*point);
            removeAvailableField(stringPoint);
            gameScene->addItem(wall);
            putFieldActorsinMap(wall);

            n--;
        }
    }
    double x = randomNumber.bounded(15);
    double y = randomNumber.bounded(15);

    while(x == y) {
        y = randomNumber.bounded(15);
    }

    int random = static_cast<int>(x);
    int random2 = static_cast<int>(y);
    _powerUpTime = random;
    _doorTime = random2;

    int m = gameContext->getLevel() + 4;
    _ghostCounter = gameContext->getLevel() + 4;

    while(m) {
        QPoint *point = new QPoint(randomNumber.bounded(sizes::COLUMNS) * sizes::FIELDSIZE
                                   , randomNumber.bounded(sizes::ROWS) * sizes::FIELDSIZE);
        if(getAvailableFields().contains(GameManager::makeStringPointStatic(point))) {
            Ghost *ghost = GhostFactory::getFactoryObject(point);
            gameScene->addItem(ghost);
            putFieldActorsinMap(ghost);
            m--;
        }
    }

    insertAvailableField(GameManager::makeStringPointStatic(new QPoint(0,0)));
    insertAvailableField(GameManager::makeStringPointStatic(new QPoint(0,sizes::FIELDSIZE)));
    insertAvailableField(GameManager::makeStringPointStatic(new QPoint(sizes::FIELDSIZE,0)));

    gameScene->addItem(_bomberman);
    putFieldActorsinMap(_bomberman);
}

void GameManager::makeBomb()
{
    if (getGameContext()->getNumberOfBombs() > 0) {
        Bomb *bomb = new Bomb(QPoint(_bomberman->getPosition().x(), _bomberman->getPosition().y()));

        getGameContext()->removeBomb();
        _gameScene->addItem(bomb);
        removeAvailableField(bomb->getStringPosition());
        putFieldActorsinMap(bomb);
    }
}

QSet<QString> GameManager::getAvailableFields() const
{
    return _availableFields;
}


void GameManager::decresePowerUpTime()
{
    _powerUpTime--;
}

int GameManager::getPowerUpTime()
{
    return _powerUpTime;
}

void GameManager::decreseDoorTime()
{
    _doorTime--;
}

int GameManager::getDoorTime()
{
    return _doorTime;
}

void GameManager::decreseGhostCounter()
{
    _ghostCounter--;
}

int GameManager::getGhostCounter()
{
    return _ghostCounter;
}

void GameManager::insertAvailableField(QString stringPosition)
{
    _availableFields.insert(stringPosition);
}

void GameManager::removeAvailableField(QString stringPosition)
{
    _availableFields.remove(stringPosition);
}

QString GameManager::makeStringPointStatic(QPoint *point)
{
    return QString(QString::number(point->x()) + ":" + QString::number(point->y()));
}

QString GameManager::makeStringPointStatic(const QPoint *point)
{
    return QString(QString::number(point->x()) + ":" + QString::number(point->y()));
}

GameContext *GameManager::getGameContext()
{
    return _gameContext;
}

void GameManager::setGameContext(GameContext *gameContext)
{
    _gameContext = gameContext;
}

bool GameManager::resetGameManager()
{
//    _availableFields.clear();
//    delete &_availableFields;

//    delete _gameScene;

//    _fieldActorsMap.clear();

    return true;
}

bool GameManager::bombermanKill()
{
    qInfo() << "bombermanKill!";
    _bomberman->removeObjectFromScreen(false);
    return true;
}

QMap<QString, Field *> GameManager::getFieldActorsMap()
{
    return _fieldActorsMap;
}

Field *GameManager::getField(QString stringPosition)
{
    auto it = _fieldActorsMap.find(stringPosition);
    if (it != _fieldActorsMap.end()) {
        return it.value();
    }
    return nullptr;
}

void GameManager::putFieldActorsinMap(Actionable *actionable)
{
    Field * field = getField(actionable->getStringPosition());
    if (field != nullptr) {
        field->addActionableFieldToList(actionable);
    }
}

void GameManager::popFieldActorsFromMap(Actionable *actionable)
{
    Field * field = getField(actionable->getStringPosition());
    if (field != nullptr) {
        field->removeActionableFromList(actionable);
    }
}

void GameManager::keyPressEvent(QKeyEvent *event)
{
    bool moved = false;
    if (_bomberman->isAvailableToMove()) {
        switch(event->key())
        {
            case Qt::Key_W:
                moved = _bomberman->moveIfAvailable(Direction::UP);
                break;
            case Qt::Key_A:
                moved = _bomberman->moveIfAvailable(Direction::LEFT);
                break;
            case Qt::Key_S:
                 moved = _bomberman->moveIfAvailable(Direction::DOWN);
                break;
            case Qt::Key_D:
                moved = _bomberman->moveIfAvailable(Direction::RIGHT);
                break;
        }
        if (moved) {
            _bomberman->setAvailableToMove(false);
        }
    }
}

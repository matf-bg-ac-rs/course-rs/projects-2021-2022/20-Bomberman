#include "../include/door.hpp"

Door::Door(QPoint position, QObject *parent)
    : Hidden(position, EntityType::DOOR, parent)
{
    setPos(position.x(), position.y());
    setPixmap(QPixmap(":/images/images/door.png"));
}

Door::~Door()
{

}

Actionable* Door::onAction(Actionable *actionable)
{
    return nullptr;
}

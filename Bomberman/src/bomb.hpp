#ifndef BOMB_HPP
#define BOMB_HPP

#include <QObject>
#include <QTimer>

#include <src/interfaces/actionable.hpp>
#include <helper/bombermantimer.hpp>

class Bomb: public Actionable
{

public:
    Bomb(QPoint position, QObject *parent = nullptr);
    ~Bomb();
    Actionable* onAction(Actionable *actionable) override;
    void createExplotion(QPoint *point);
    bool canContinue(QPoint point);
    bool canContinue(QPoint *position, const QString stringPosition);
    void createTimer();

public slots:
    void objectDie();

private:
    BombermanTimer *_bombermanTimer;
};

#endif // BOMB_HPP

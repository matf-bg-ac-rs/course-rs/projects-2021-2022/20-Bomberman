#include "../include/entity.hpp"

Entity::Entity(QPoint position, EntityType entityType, QObject *parent)
    : QObject(parent)
    , _position(position)
    , _entityType(entityType)
{
    createStringPosition();
}

QString Entity::getStringPosition() const
{
    return _stringPosition;
}

void Entity::createStringPosition()
{
    _stringPosition = *new QString(QString::number(_position.x()) + ":" + QString::number(_position.y()));

}

QPoint Entity::getPosition() const // return QPoint* ?
{
    return _position;
}

EntityType Entity::getEntityType() const
{
    return _entityType;
}

Entity::~Entity()
{

}

//Entity::Entity(const Entity &other)
//{

//}

//Entity::Entity(Entity &&other) noexcept
//{

//}

//Entity &Entity::operator=(const Entity &other)
//{

//}
//// Ideja ove funkcije je da omoguci sledece (pseudokod):
//    // list xs([1, 2, 3, 4, 5])
//    // Odnosno, da se privremena lista [1, 2, 3, 4, 5] ne kopira pa potom obrise,
//    // vec da se direktno preuzme pri konstrukciji nase liste.
//    list::list(list &&other) noexcept
//        // Pozivom std::move "krademo" implementaciju od liste koja se prosledjuje.
//        : m_start(std::move(other.m_start))
//        // Za primitivne tipove, kao sto je size_t, nema potrebe zvati std::move.
//        , m_size(other.m_size)
//    {
//        // Posto smo "ukrali" unique_ptr iz druge liste (cime se on postavlja na nullptr),
//        // onda je neophodno da rucno postavimo da je duzina druge liste 0,
//        // kako bi ostali kod radio korektno (poput metoda size()).
//        other.m_size = 0;
//    }

//    // Alternativna implementacija:
//    //   - Pozivamo podrazumevani konstruktor koji ce inicijalizovati podatke ove liste:
//    //     m_start = nullptr, m_size = 0u (pogledati definicije ovih atributa u list.hpp)
//    //   - A zatim vrsimo razmenu podataka izmedju privremene liste i ove liste
//    // list::list(list &&other) noexcept
//    //     : list()
//    // {
//    //     std::swap(m_start, other.m_start);
//    //     std::swap(m_size, other.m_size);
//    // }

//    list &list::operator=(const list &other)
//    {
//        // Ovde demonstriramo tzv. "Copy and Swap" idiom.
//        // Da bi operator dodele bio bezbedan prilikom pojave izuzetaka,
//        // obavezno je implementirati ga na ovaj nacin:

//        // 1. Pravi se privremena kopija objekta (poziva se konstruktor kopije)
//        auto temp(other);
//        // 2. Zamene se sve clanske promenljive u *this i privremenoj kopiji
//        std::swap(temp.m_start, m_start);
//        std::swap(temp.m_size, m_size);
//        // Operator dodele uvek vraca referencu na tekuci objekat,
//        // kako bismo mogli ulancavati ovaj operator.
//        return *this;
//    }

//    list &list::operator=(list &&other) noexcept
//    {
//        // Ovde demonstriramo tzv. "Move and Swap" idiom.
//        // Ideja je ista kao u "Copy and Swap" idiomu:

//        // 1. Poziva se konstruktor sa semantikom pomeranja
//        auto temp(std::move(other));
//        // 2. Zamene se sve clanske promenljive u *this i privremenoj kopiji
//        std::swap(temp.m_start, m_start);
//        std::swap(temp.m_size, m_size);
//        // Operator dodele uvek vraca referencu na tekuci objekat,
//        // kako bismo mogli ulancavati ovaj operator.
//        return *this;
//    }

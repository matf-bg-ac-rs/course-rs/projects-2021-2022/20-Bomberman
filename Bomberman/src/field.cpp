#include <QGraphicsScene>

#include "../include/field.hpp"

Field::Field(QPoint position, QObject *parent)
    : Entity(position, EntityType::FIELD, parent)
{
    setPos(position.x(), position.y());
    setPixmap(QPixmap(":/images/images/field.png"));

}

QList<Actionable *> Field::getActionableList() const
{
    return _actionableList;
}

void Field::removeActionableFromList(Actionable *actionable)
{
     QList<Actionable *>::ConstIterator iterator = _actionableList.begin();

     while (iterator != _actionableList.end()) {
         Actionable *tmp = *iterator ;
         if (tmp->getEntityType() == actionable->getEntityType()) {
           _actionableList.erase(iterator);
            break;
         }

         ++iterator;
     }
}


void Field::addActionableFieldToList(Actionable *actionable)
{
    if(Field::fieldEntered(actionable)) {
        _actionableList.push_back(actionable);
    }
}

bool Field::fieldEntered(Actionable *actionable)
{
    QList<Actionable *>::iterator iterator = _actionableList.begin();
    QList<Actionable *> toDelete;
    bool actionableAlive = true;
    while (iterator != _actionableList.end()) {
        Actionable *tmp = *iterator;
        Actionable *forDelete = actionable->onAction(tmp);

        if (nullptr != forDelete) {
            if (forDelete == actionable) {
                actionableAlive = false;
            }
            toDelete.push_back(forDelete);
        }
        ++iterator;
    }
    if (toDelete.size() > 0) {
        QMutableListIterator<Actionable *> qmli(toDelete);
        while (qmli.hasNext()) {
            Actionable* act = qmli.next();
            switch (act->getEntityType()) {
                case EntityType::BOMBERMAN:
                case EntityType::GHOST:
                case EntityType::POWERUP:
                case EntityType::EXPLOSION:
                    act->removeObjectFromScreen(false);
                    break;
                case EntityType::BOMB:
                case EntityType::WALL:
                    act->removeObjectFromScreen(true);
                    break;
                default:
                    break;
            }

            qmli.remove();
        }
    }

    return actionableAlive;
}

Field::~Field()
{
}


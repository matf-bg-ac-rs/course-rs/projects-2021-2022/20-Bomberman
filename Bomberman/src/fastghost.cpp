#include <include/fastghost.hpp>
#include <include/gameManager.hpp>

FastGhost::FastGhost(QPoint position, QObject *parent, int movement)
    : Ghost(position, EntityType::GHOST, parent, movement)
{
    setPos(position.x(), position.y());
    setPixmap(QPixmap(":/images/images/fastGhost.png"));
    createTimer();
    setPointsWorth(500);
}

FastGhost::~FastGhost()
{
}

void FastGhost::useTimer()
{
    bool moveWillSucceed = false;
    QSet<Direction> usedDirections = *new QSet<Direction>;
    Direction nextTry = lastTry;
    while (!moveWillSucceed && usedDirections.size() < 4) {
        nextTry = randomDirection();
        usedDirections.insert(nextTry);
        moveWillSucceed = isAvailable(nextTry);
    }

    moveIfAvailable(nextTry);
    getBombermanTimer()->startTimer();
}

Direction FastGhost::randomDirection()
{
    double x = rand()/static_cast<double>(RAND_MAX);
    int random = static_cast<int>(x*4);

    switch(random)
    {
        case 0:
            return Direction::LEFT;
        case 1:
            return Direction::RIGHT;
        case 2:
            return Direction::UP;
        default:
            return Direction::DOWN;
    }
}

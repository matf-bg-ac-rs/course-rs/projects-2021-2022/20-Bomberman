#ifndef EXPLOSION_H
#define EXPLOSION_H

#include <src/interfaces/actionable.hpp>
#include <QDebug>
#include <helper/bombermantimer.hpp>



class Explosion : public Actionable
{
public:
    Explosion(QPoint position, QObject *parent = nullptr);
    ~Explosion() override;
    Actionable* onAction(Actionable *actionable) override;
    void createTimer();

public slots:
    void objectDie();

private:
    BombermanTimer *_bombermanTimer;
};

#endif // EXPLOSION_H

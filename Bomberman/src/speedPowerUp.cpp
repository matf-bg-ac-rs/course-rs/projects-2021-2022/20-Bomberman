#include "../include/speedPowerUp.hpp"

SpeedPowerUp::SpeedPowerUp(QPoint position, QObject *parent)
    : PowerUp(position, parent)
{
    setPos(position.x(), position.y());
    setPixmap(QPixmap(":/images/images/speedPowerUp.png"));

}

SpeedPowerUp::~SpeedPowerUp()
{

}

void SpeedPowerUp::usePower()
{
    GameManager::getInstance()->getGameContext()->removeFromCurrentSpeed(_speedConstant);
}

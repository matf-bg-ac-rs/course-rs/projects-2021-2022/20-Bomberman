#include "../include/wall.hpp"
#include <src/interfaces/powerUp.hpp>
#include <include/bombPowerUp.hpp>
#include <include/gameManager.hpp>
#include <include/door.hpp>
#include <helper/powerupfactory.hpp>

Wall::Wall(QPoint position, QObject *parent)
    : Actionable(position, EntityType::WALL, parent)
{
    setPos(position.x(), position.y());
    setPixmap(QPixmap(":/images/images/destroyableBlock.png"));
}

Actionable* Wall::onAction(Actionable *actionable)
{
    switch (actionable->getEntityType()) {
        case EntityType::EXPLOSION:
            return this;
        default:
            return nullptr;
    }
}


Wall::~Wall()
{
    if(GameManager::getInstance()->getPowerUpTime() > 0) {
        GameManager::getInstance()->decresePowerUpTime();
    } else if (GameManager::getInstance()->getPowerUpTime() == 0) {
        PowerUp* powerUp = PowerUpFactory::getFactoryObject(&_position);
        GameManager::getInstance()->getGameScene()->addItem(powerUp);
        GameManager::getInstance()->putFieldActorsinMap(powerUp);
        GameManager::getInstance()->decresePowerUpTime();
    }

    if(GameManager::getInstance()->getDoorTime() > 0) {
        GameManager::getInstance()->decreseDoorTime();
    } else if(GameManager::getInstance()->getDoorTime() == 0) {
        Door* door = new Door(getPosition());
        GameManager::getInstance()->getGameScene()->addItem(door);
        GameManager::getInstance()->putFieldActorsinMap(door);
        GameManager::getInstance()->decreseDoorTime();
    }
}

#include "bomb.hpp"
#include "explosion.hpp"
#include "../include/settings.hpp"


#include <QDebug>
#include <QObject>
#include <QPoint>

#include <include/gameManager.hpp>


Bomb::Bomb(QPoint position, QObject *parent)
    : Actionable(position, EntityType::BOMB, parent)
{
    setPos(position.x(), position.y());
    setPixmap(QPixmap(":/images/images/bomb.png"));
    createTimer();
}

Actionable* Bomb::onAction(Actionable *actionable)
{
    switch (actionable->getEntityType()) {
        case EntityType::BOMBERMAN:
            return nullptr;
        case EntityType::EXPLOSION:
            return actionable->onAction(this);
        default:
            return nullptr;
    }
}

void Bomb::createTimer()
{
    _bombermanTimer = new BombermanTimer(3000);
    connect(_bombermanTimer->_timer, &QTimer::timeout, this, &Bomb::objectDie);
    _bombermanTimer->startTimer();
}

Bomb::~Bomb()
{
    delete _bombermanTimer;
    int explotionRange = GameManager::getInstance()->getGameContext()->getBombExplotionRange();
    QPoint point(QPoint(getPosition().x(), getPosition().y()));
    createExplotion(&point);

    bool left = true;
    bool right = true;
    bool up = true;
    bool down = true;

    for(int i = 1; i < explotionRange + 1; i++) {
        if (left) {
            left = canContinue(QPoint(getPosition().x() - i * sizes::FIELDSIZE, getPosition().y()));
        }
        if (right) {
            right = canContinue(QPoint(getPosition().x() + i * sizes::FIELDSIZE, getPosition().y()));
        }
        if (up) {
            up = canContinue(QPoint(getPosition().x(), getPosition().y() + i * sizes::FIELDSIZE));
        }
        if (down) {
            down = canContinue(QPoint(getPosition().x(), getPosition().y() - i * sizes::FIELDSIZE));
        }
    }
    GameManager::getInstance()->getGameContext()->addOneBomb();
}

void Bomb::objectDie()
{
    this->removeObjectFromScreen(true);
}

bool Bomb::canContinue(QPoint point) {
    return canContinue(&point, QString(GameManager::makeStringPointStatic(&point)));
}

bool Bomb::canContinue(QPoint *position, const QString stringPosition) {
//    Field* field = GameManager::getInstance()->findFieldActorsList(stringPosition);
    Field* field = GameManager::getInstance()->getField(stringPosition);
    if (field == nullptr) {
        return false;
    } else {
        bool can = field->getActionableList().size() == 0;
        createExplotion(position);
        return can;
    }
}

void Bomb::createExplotion(QPoint *point)
{
    Explosion *explosion = new Explosion(*point);
    explosion->createObjectOnScreen(false);
}





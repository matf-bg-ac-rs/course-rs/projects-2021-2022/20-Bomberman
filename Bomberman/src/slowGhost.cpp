#include "../include/slowGhost.hpp"

#include <include/gameManager.hpp>

SlowGhost::SlowGhost(QPoint position, QObject *parent, int speed)
    : Ghost(position, EntityType::GHOST, parent, speed)
{
    setPos(position.x(), position.y());
    setPixmap(QPixmap(":/images/images/slowGhost.png"));
    createTimer();
    setPointsWorth(300);
}

SlowGhost::~SlowGhost()
{
}

void SlowGhost::useTimer()
{
    bool moveSucceded = isAvailable(lastTry);
    QSet<Direction> usedDirections = *new QSet<Direction>;
    while (!moveSucceded && usedDirections.size() < 4) {
        Direction nextTry = randomDirection();
        usedDirections.insert(nextTry);
        while(nextTry == lastTry)
        {
            nextTry = randomDirection();
            usedDirections.insert(nextTry);
        }
        moveSucceded = isAvailable(nextTry);
        lastTry = nextTry;
    }

    moveIfAvailable(lastTry);
    getBombermanTimer()->startTimer();
}

Direction SlowGhost::randomDirection()
{
    double x = rand()/static_cast<double>(RAND_MAX);
    int random = static_cast<int>(x*4);

    switch(random)
    {
        case 0:
            return Direction::LEFT;
        case 1:
            return Direction::RIGHT;
        case 2:
            return Direction::UP;
        default:
            return Direction::DOWN;
    }
}

#include "actionable.hpp"

#include <include/gameManager.hpp>

Actionable::Actionable(QPoint position, EntityType entityType, QObject *parent)
    : Entity(position, entityType, parent)
{
}
/*
 * Kreira objekat na ekran
 * blocker - da li objekat ogranicava kretanje ili ne, da li se preko njega moze proci
 */
bool Actionable::createObjectOnScreen(bool blocker)
{
    if(blocker) {
        GameManager::getInstance()->removeAvailableField(getStringPosition());
    }
    GameManager::getInstance()->putFieldActorsinMap(this);
    GameManager::getInstance()->getGameScene()->addItem(this);
    return true;
}

bool Actionable::removeObjectFromScreen(bool blocker)
{
    if(blocker) {
        GameManager::getInstance()->insertAvailableField(getStringPosition());
    }
    GameManager::getInstance()->popFieldActorsFromMap(this);
    this->~Actionable();
    return true;
}

Actionable::~Actionable()
{

}

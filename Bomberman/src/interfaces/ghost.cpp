#include "../../src/interfaces/ghost.hpp"

#include <include/fastghost.hpp>
#include <include/gameManager.hpp>

Ghost::Ghost(QPoint position, EntityType entityType, QObject *parent, int speed)
    : Movable(position, entityType, parent)
{
    setSpeed(speed);
}

Ghost::~Ghost()
{
    GameManager::getInstance()->decreseGhostCounter();
    GameManager::getInstance()->getGameContext()->updateScore(getPointsWorth());
}

int Ghost::getPointsWorth() const
{
    return _pointsWorth;
}

void Ghost::setPointsWorth(int pointsWorth)
{
    _pointsWorth = pointsWorth;
}

Ghost *Ghost::createGhost(QPoint *point)
{
    int number = rand() % 2;
    if(number == 0) {
        return new SlowGhost(*point);
    } else {
        return new FastGhost(*point);
    }
}

Actionable * Ghost::onAction(Actionable *actionable)
{
    switch (actionable->getEntityType()) {
    case EntityType::BOMBERMAN:
        return actionable->onAction(this);
    case EntityType::EXPLOSION:
        return actionable->onAction(this);
    default:
        return nullptr;
    }
}



#include "../../src/interfaces/powerUp.hpp"

#include <include/bombPowerUp.hpp>
#include <include/bombPowerUp.hpp>
#include <include/explosionPowerUp.hpp>
#include <include/gameManager.hpp>
#include <include/healthPowerUp.hpp>
#include <include/speedPowerUp.hpp>

PowerUp::PowerUp(QPoint position, QObject *parent)
    : Hidden(position, EntityType::POWERUP, parent)
{
//    connect(GameManager::getInstance(), &GameManager::powerUpUsed, this, &PowerUp::destroy);a
}

PowerUp::~PowerUp()
{
    GameManager::getInstance()->getGameContext()->updateScore(getPointsWorth());
}

PowerUp *PowerUp::createPowerUp(QPoint *point)
{
    int number = rand() % 4;
    if(number == 0) {
        return new BombPowerUp(*point);
    } else if(number == 1) {
        return new ExplosionPowerUp(*point);
    } else if(number == 2) {
        return new SpeedPowerUp(*point);
    } else {
        return new HealthPowerUp(*point);
    }
}

int PowerUp::getPointsWorth() const
{
    return _pointsWorth;
}

Actionable* PowerUp::onAction(Actionable *actionable) {
    switch (actionable->getEntityType()) {
        case EntityType::BOMBERMAN:
            this->usePower();
            return this;
        case EntityType::GHOST:
            return nullptr;
        case EntityType::POWERUP:
            return nullptr;
        case EntityType::DOOR:
            return nullptr;
        case EntityType::EXPLOSION:
            return this;
        default:
            return nullptr;
    }
}

void PowerUp::destroy()
{
    this->removeObjectFromScreen(false);
}

#ifndef ACTIONABLE_HPP
#define ACTIONABLE_HPP

#include <include/entity.hpp>

#include <QString>

class Actionable: public Entity
{
public:
    Actionable(QPoint position, EntityType entityType, QObject *parent = nullptr);
    virtual Actionable* onAction(Actionable *actionable) = 0;
    bool createObjectOnScreen(bool blocker);
    bool removeObjectFromScreen(bool blocker);
    virtual ~Actionable();


private:
};

#endif // ACTIONABLE_HPP

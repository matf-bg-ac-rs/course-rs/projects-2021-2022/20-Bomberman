#ifndef POWERUP_HPP
#define POWERUP_HPP

#include "hidden.hpp"
#include <include/gameManager.hpp>

class PowerUp : public Hidden
{
public:
    PowerUp(QPoint position, QObject *parent = nullptr);
    ~PowerUp();

    static PowerUp* createPowerUp(QPoint* point);
    int getPointsWorth() const;

    Actionable* onAction(Actionable *actionable) override;

    virtual void usePower() = 0;
public slots:
    void destroy();

private:
    int _pointsWorth = 100;
};

#endif // POWERUP_HPP

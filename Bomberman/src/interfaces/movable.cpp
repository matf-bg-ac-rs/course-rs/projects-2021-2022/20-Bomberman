#include "../../src/interfaces/movable.hpp"
#include "../../include/settings.hpp"
#include <include/gameManager.hpp>

Movable::Movable(QPoint position, EntityType entityType, QObject *parent)
    : Actionable(position, entityType, parent)
{
}

Movable::~Movable()
{

}

void Movable::setPosition(QPoint newPosition)
{
    setPos(newPosition);
    createStringPosition();
}

bool Movable::move(Direction direction)
{
    GameManager::getInstance()->popFieldActorsFromMap(this);

    switch (direction) {
        case Direction::DOWN:
            this->_position += (QPoint(0, sizes::FIELDSIZE));
            break;
         case Direction::UP:
            this->_position += (QPoint(0, -sizes::FIELDSIZE));
            break;
         case Direction::LEFT:
            this->_position += (QPoint(-sizes::FIELDSIZE, 0));
            break;
        case Direction::RIGHT:
            this->_position += (QPoint(sizes::FIELDSIZE, 0));
            break;
    }
    setPosition(this->getPosition());

    GameManager::getInstance()->putFieldActorsinMap(this);
    return true;

}

bool Movable::moveIfAvailable(Direction direction)
{
    if (isAvailable(direction))
    {
        return move(direction);
    }
    return false;
}

bool Movable::isAvailable(Direction direction){
    QPoint checkPosition = this->_position;
    switch(direction)
    {
        case Direction::UP:
            checkPosition += (QPoint(0, -sizes::FIELDSIZE));
            break;
        case Direction::DOWN:
            checkPosition += (QPoint(0, sizes::FIELDSIZE));
            break;
        case Direction::LEFT:
            checkPosition += (QPoint(-sizes::FIELDSIZE, 0));
            break;
        case Direction::RIGHT:
            checkPosition += (QPoint(sizes::FIELDSIZE, 0));
            break;
    }
    return GameManager::getInstance()->getAvailableFields()
            .contains(GameManager::makeStringPointStatic(&checkPosition));
}

BombermanTimer *Movable::getBombermanTimer()
{
    return _bombermanTimer;
}

void Movable::createTimer()
{
    _bombermanTimer = new BombermanTimer(getSpeed());
    connect(_bombermanTimer->_timer, &QTimer::timeout, this, &Movable::useTimer);
    timerStart();
}

void Movable::timerStart()
{
    _bombermanTimer->startTimer();
}

void Movable::destroyTimer()
{
    _bombermanTimer->_timer->stop();
    _bombermanTimer->~BombermanTimer();

}

QPoint *Movable::getToPosition()
{
    return _toPosition;
}

void Movable::setToPosition(QPoint *toPosition)
{
    _toPosition = toPosition;
}

int Movable::getSpeed()
{
    return _speed;
}

void Movable::setSpeed(int speed)
{
    _speed = speed;
}

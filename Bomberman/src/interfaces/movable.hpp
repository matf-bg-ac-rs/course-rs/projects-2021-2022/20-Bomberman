#ifndef MOVABLE_HPP
#define MOVABLE_HPP

#include "actionable.hpp"

#include <QPoint>
#include <helper/bombermantimer.hpp>
#include <helper/direction.hpp>

class Movable : public Actionable
{

public:
    Movable(QPoint position, EntityType entityType, QObject *parent = nullptr);
    ~Movable();
    bool moveIfAvailable(Direction direction);
    bool move(Direction direction);
    void setPosition(QPoint newPosition);
    bool isAvailable(Direction direction);
    QPoint* getPointFromDirection(Direction direction);
    BombermanTimer* getBombermanTimer();
    void createTimer();
    void timerStart();
    void destroyTimer();


    QPoint* getToPosition();
    void setToPosition(QPoint* toPosition);

    int getSpeed();
    void setSpeed(int speed);

public slots:
    virtual void useTimer() = 0;

private:
    int _speed;
    QPoint* _toPosition;
    BombermanTimer* _bombermanTimer;

};

#endif // MOVABLE_HPP

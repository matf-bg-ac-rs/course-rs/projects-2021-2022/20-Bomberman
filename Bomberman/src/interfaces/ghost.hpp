#ifndef GHOST_HPP
#define GHOST_HPP

#include <src/interfaces/movable.hpp>

#include <helper/bombermantimer.hpp>

class Ghost : public Movable
{
public:
    Ghost(QPoint position, EntityType entityType, QObject *parent = nullptr, int speed = 3000);
    Actionable* onAction(Actionable *actionable) override;
    virtual ~Ghost();
    int getPointsWorth() const;
    void setPointsWorth(int pointsWorth);

    static Ghost* createGhost(QPoint *point);

private:
     int _pointsWorth;
};


#endif // GHOST_HPP

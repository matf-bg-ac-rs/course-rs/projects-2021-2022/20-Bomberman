#ifndef FACTORY_HPP
#define FACTORY_HPP

#include <helper/entitytype.hpp>

class Factory
{
public:
    Factory();
    virtual Factory* getFactory(EntityType entityType) const = 0;
};

#endif // FACTORY_HPP

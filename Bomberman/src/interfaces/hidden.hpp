#ifndef HIDDEN_HPP
#define HIDDEN_HPP

#include "actionable.hpp"

class Hidden : public Actionable
{
public:
    Hidden(QPoint position, EntityType entityType, QObject *parent = nullptr);
    ~Hidden();

    void show();

    bool _hidden;
};

#endif // HIDDEN_HPP

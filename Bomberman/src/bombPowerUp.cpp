#include "../include/bombPowerUp.hpp"
#include <include/gameManager.hpp>

BombPowerUp::BombPowerUp(QPoint position, QObject *parent)
    : PowerUp(position, parent)
{
    setPos(position.x(), position.y());
    setPixmap(QPixmap(":/images/images/bombPowerUp.png"));
}

BombPowerUp::~BombPowerUp()
{

}

void BombPowerUp::usePower()
{
    GameManager::getInstance()->getGameContext()->addOneBomb();
}

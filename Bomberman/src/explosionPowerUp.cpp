#include "../include/explosionPowerUp.hpp"
#include <include/gameManager.hpp>

ExplosionPowerUp::ExplosionPowerUp(QPoint position, QObject *parent)
    : PowerUp(position, parent)
{
    setPos(position.x(), position.y());
    setPixmap(QPixmap(":/images/images/explosionPowerUp.png"));
}

ExplosionPowerUp::~ExplosionPowerUp()
{

}

void ExplosionPowerUp::usePower()
{
    GameManager::getInstance()->getGameContext()->addOneExplotionRange();

}
